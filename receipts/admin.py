from django.contrib import admin
from receipts.models import Receipt, ExpenseCategory, Account

# Register your models here.

class ReceiptAdmin(admin.ModelAdmin):
  pass

admin.site.register(Receipt, ReceiptAdmin)
admin.site.register(ExpenseCategory, ReceiptAdmin)
admin.site.register(Account, ReceiptAdmin)
