from receipts.views import receipts_list, create_receipt, create_account, create_category, category_list, account_list
from django.urls import path

urlpatterns = [
  path("", receipts_list, name="home"),
  path("create/", create_receipt, name="create_receipt"),
  path("categories/", category_list, name="categories"),
  path("accounts/", account_list , name="accounts"),
  path("categories/create/", create_category, name="create_category"),
  path("accounts/create/", create_account, name="create_account"),
]